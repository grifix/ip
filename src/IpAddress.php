<?php

declare(strict_types=1);

namespace Grifix\Ip;

use Grifix\Ip\Exceptions\InvalidIpAddressException;

final class IpAddress
{
    public function __construct(private readonly string $value)
    {
        if (false === filter_var($value, FILTER_VALIDATE_IP)) {
            throw new InvalidIpAddressException();
        }
    }

    public static function create(string $value): self
    {
        return new self($value);
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
