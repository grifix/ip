<?php

declare(strict_types=1);

namespace Grifix\Ip\Exceptions;

final class InvalidIpAddressException extends \Exception
{

    public function __construct()
    {
        parent::__construct('Invalid IP address!');
    }
}
