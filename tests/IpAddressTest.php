<?php

declare(strict_types=1);

namespace Grifix\Ip\Tests;

use Grifix\Ip\Exceptions\InvalidIpAddressException;
use Grifix\Ip\IpAddress;
use PHPUnit\Framework\TestCase;

final class IpAddressTest extends TestCase
{
    public function testItCreates(): void
    {
        $value = '127.0.0.1';
        $ip = IpAddress::create($value);
        self::assertEquals($value, $ip->toString());
        self::assertEquals($value, (string)$ip);
    }

    /**
     * @dataProvider wrongIpAddressDateProvider
     */
    public function testItDoesNotCreate(string $value): void
    {
        $this->expectException(InvalidIpAddressException::class);
        $this->expectExceptionMessage('Invalid IP address!');
        IpAddress::create($value);
    }

    public function wrongIpAddressDateProvider(): array
    {
        return [
            [
                '127.0.0.1.2',
                'wrong ip address',
                '1000.1000.1000.1000',
                '127.0.0.1000'
            ]
        ];
    }
}
